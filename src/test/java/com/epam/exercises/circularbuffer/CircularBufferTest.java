package com.epam.exercises.circularbuffer;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class CircularBufferTest {

    private static int BUFFER_CAPACITY = 10;
    private static int OVER_BUFFER_CAPACITY = BUFFER_CAPACITY + 1;

    private CircularBuffer<String> buffer = new CircularBuffer<>(String.class, BUFFER_CAPACITY);
    private CircularBuffer<Integer> intBuffer = new CircularBuffer<>(Integer.class, BUFFER_CAPACITY);

    @Test
    void put_whenFirstElementIsAdded_thenPuttingSuccess() {

        buffer.put("String");
        assertEquals("String", buffer.get());
    }

    @Test
    void put_whenAddMoreElementsThanCapacity_thenThrowRuntimeException() {

        fulfillTheBuffer();

        assertThrows(RuntimeException.class, () -> buffer.put("a"));
    }

    @Test
    void get_whenExecuteGet_thenTailElementIsReturned() {

        buffer.put("one");
        buffer.put("two");

        assertEquals("one", buffer.get());
    }

    @Test
    void get_whenExecuteGetOnEmptyBuffer_thenRuntimeExceptionIsThrown() {

        assertThrows(RuntimeException.class, () -> buffer.get());
    }

    @Test
    void toObjectArray_whenExecute_thenCorrectObjectArrayIsReturned() {
        fillBufferWithThreeElements();

        assertArrayEquals(new Object[]{"one", "two", "three"}, buffer.toObjectArray());
    }

    @Test
    void toObjectArray_whenExecute_thenFirstElementIsTailElement() {
        fillBufferWithThreeElements();

        assertEquals("one", buffer.toObjectArray()[0]);
    }

    @Test
    void toObjectArray_whenExecute_thenLengthOfReturnedArrayEqualsToBufferSize() {

        fillBufferWithThreeElements();
        assertEquals(3, buffer.toObjectArray().length);
    }

    @Test
    void toArray_whenExecute_thenCorrectStringArrayIsReturned() {
        fillBufferWithThreeElements();

        assertArrayEquals(new String[]{"one", "two", "three"}, buffer.toObjectArray());
    }

    @Test
    void toArray_whenExecute_thenFirstElementIsTailElement() {
        fillBufferWithThreeElements();

        assertEquals("one", buffer.toObjectArray()[0]);
    }

    @Test
    void toOArray_whenExecute_thenLengthOfReturnedArrayEqualsToBufferSize() {

        fillBufferWithThreeElements();
        assertEquals(3, buffer.toObjectArray().length);
    }

    @Test
    void asList_whenExecute_thenCorrectListIsReturned() {
        fillBufferWithThreeElements();
        assertEquals("one", buffer.asList().get(0));
        assertEquals("two", buffer.asList().get(1));
        assertEquals("three", buffer.asList().get(2));
    }

    @Test
    void asList_whenExecute_thenFirstElementIsTailElement() {
        fillBufferWithThreeElements();

        assertEquals("one", buffer.asList().get(0));
    }

    @Test
    void asList_whenExecute_thenLengthOfReturnedListEqualsToBufferSize() {

        fillBufferWithThreeElements();
        assertEquals(3, buffer.asList().size());
    }

    @Test
    void addAll_whenNotTooLargeListIsAdded_thenPuttingSuccess() {

        List<Integer> intList = new ArrayList<>();
        IntStream.iterate(0, x -> x + 1)
                .limit(BUFFER_CAPACITY)
                .forEach(intList::add);

        intBuffer.addAll(intList);

        intList.stream()
                .mapToInt(x -> x)
                .forEach(element -> assertEquals(element, intBuffer.get()));
    }

    @Test
    void addAll_whenListContainsMoreElementsThanCapacity_thenThrowRuntimeException() {
        List<Integer> intList = new ArrayList<>();
        IntStream.iterate(0, x -> x + 1)
                .limit(OVER_BUFFER_CAPACITY)
                .forEach(intList::add);

        assertThrows(RuntimeException.class, () -> intBuffer.addAll(intList));
    }

    @Test
    void sort_whenExecute_thenBufferBecomeSorted() {
        intBuffer.put(1);
        intBuffer.put(3);
        intBuffer.put(2);

        intBuffer.sort((t1, t2) -> {
                    if (t1.equals(t2)) {
                        return 0;
                    } else if (t1 > t2) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
        );
        assertEquals(1, intBuffer.get());
        assertEquals(2, intBuffer.get());
        assertEquals(3, intBuffer.get());
    }

    @Test
    void isEmpty_whenNoElementsAreInBuffer_thenReturnTrue() {
        CircularBuffer<String> emptyBuffer = new CircularBuffer<>(String.class, 2);
        assertTrue(emptyBuffer.isEmpty());
    }

    @Test
    void isEmpty_whenAnyElementsAreInBuffer_thenReturnFalse() {
        fillBufferWithThreeElements();
        assertFalse(buffer.isEmpty());
    }

    private void fulfillTheBuffer() {

        IntStream.iterate(1, x -> x + 1)
                .limit(BUFFER_CAPACITY)
                .forEach(x -> buffer.put(Integer.toString(x)));
    }

    private void fillBufferWithThreeElements() {
        buffer.put("one");
        buffer.put("two");
        buffer.put("three");
    }
}
