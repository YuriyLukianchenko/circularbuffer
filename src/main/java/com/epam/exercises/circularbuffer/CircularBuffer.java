package com.epam.exercises.circularbuffer;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class CircularBuffer<T> {

    private final T[] buffer;

    private final Class<T> clazz;

    private int head = 0;

    private int tail = 0;

    @SuppressWarnings("unchecked")
    public CircularBuffer(Class<T> clazz, int capacity) {
        this.buffer = (T[]) Array.newInstance(clazz, capacity);
        this.clazz = clazz;
    }

    public void put(T t) {
        if (!isFull()) {
            buffer[head] = t;
            moveHeadPosition();
        } else {
            throw new RuntimeException("buffer is full");
        }
    }

    public T get() {
        T t;
        if (!isEmpty()) {
            t = buffer[tail];
            buffer[tail] = null;
            moveTailPosition();
        } else {
            throw new RuntimeException("buffer is empty");
        }
        return t;
    }

    public Object[] toObjectArray() {

        int size = getSize();

        Object[] array = new Object[size];

        if (tail < head) {
            int j = 0;
            for (int i = tail; i < head; i++) {
                array[j] = buffer[i];
                j++;
            }

        } else if (size != 0) {
            int j = 0;
            for (int i = tail; i < buffer.length; i++) {
                array[j] = buffer[i];
                j++;
            }
            for (int i = 0; i < head; i++) {
                array[j] = buffer[i];
                j++;
            }
        }
        return array;
    }

    public T[] toArray() {

        int size = getSize();

        T[] array = (T[]) Array.newInstance(clazz, size);

        if (tail < head) {
            int j = 0;
            for (int i = tail; i < head; i++) {
                array[j] = buffer[i];
                j++;
            }

        } else if (size != 0) {
            int j = 0;
            for (int i = tail; i < buffer.length; i++) {
                array[j] = buffer[i];
                j++;
            }
            for (int i = 0; i < head; i++) {
                array[j] = buffer[i];
                j++;
            }
        }
        return array;
    }

    public List<T> asList() {

        int size = getSize();

        List<T> list = new ArrayList<>();

        if (tail < head) {
            for (int i = tail; i < head; i++) {
                list.add(buffer[i]);
            }

        } else if (size != 0) {
            for (int i = tail; i < buffer.length; i++) {
                list.add(buffer[i]);
            }
            for (int i = 0; i < head; i++) {
                list.add(buffer[i]);
            }
        }
        return list;
    }

    public void addAll(List<? extends T> toAdd) {
        if (toAdd.size() <= (buffer.length - getSize())) {
            toAdd.stream()
                    .forEach(element -> put(element));
        } else {
            throw new RuntimeException("list is too large");
        }
    }

    public void sort(Comparator<? super T> comparator) {
        T[] array = this.toArray();
        Arrays.sort(array, (Comparator) comparator);
        while (!isEmpty()) {
            this.get();
        }
        this.addAll(Arrays.asList(array));
    }

    public boolean isEmpty() {
        return (tail == head) && (buffer[0] == null);
    }

    private boolean isFull() {
        return (tail == head) && (buffer[0] != null);
    }

    private void moveHeadPosition() {
        if (head == buffer.length - 1) {
            head = 0;
        } else
            head++;
    }

    private void moveTailPosition() {
        if (tail == buffer.length - 1) {
            tail = 0;
        } else
            tail++;
    }

    private int getSize() {
        int size;
        if (head == tail) {
            size = isFull() ? buffer.length : 0;
        } else {
            size = Math.abs(-tail + head);
        }
        return size;
    }
}
